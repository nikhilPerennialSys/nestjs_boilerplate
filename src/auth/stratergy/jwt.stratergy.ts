import { BadRequestException, Inject, Injectable } from '@nestjs/common';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { ConfigService } from 'src/core/config/config.service';
import { User } from '../models/user.model';
import { JwtPayload } from 'src/core/constants/interfaces/other-interfaces';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    private configService: ConfigService,
    @Inject('USER_REPOSITORY')
    private readonly userRepository: typeof User,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: configService.get('JWT_SECRET'),
      ignoreExpiration: false,
    });
  }

  async validate(payload: JwtPayload) {
    const { email } = payload;
    const userExists = await this.userRepository.findOne({
      where: { email: email },
    });
    if (!userExists) {
      throw new BadRequestException('Invalid user');
    } else {
      return {
        email: payload.email,
        firstName: userExists.firstName,
        lastName: userExists.lastName,
        role: userExists.role,
      };
    }
  }
}
