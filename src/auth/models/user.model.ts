import {
  Table,
  Column,
  Model,
  BeforeCreate,
  DataType,
  HasOne,
} from 'sequelize-typescript';
import { hash } from 'bcryptjs';
import { ROLES } from 'src/core/constants/interfaces/other-interfaces';
import { UsersDetails } from './userDetails.model';

@Table
export class User extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id: number;

  @Column
  firstName: string;

  @Column
  lastName: string;

  @Column
  age: number;

  @Column({ unique: true })
  email: string;

  @Column
  password: string;

  @Column({
    type: DataType.ENUM,
    values: ['ADMIN', 'USER', 'OTHER'],
    allowNull: false,
    defaultValue: ROLES.USER,
  })
  role: string;

  @HasOne(() => UsersDetails)
  usersDetails: UsersDetails[];

  @BeforeCreate
  static async hashPasswordBeforeCreate(user: User) {
    if (user.password) {
      const hashedPassword = await hash(user.password, 14);
      user.password = hashedPassword;
    }
  }
}
