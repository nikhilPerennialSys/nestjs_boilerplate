import {
  Table,
  Column,
  Model,
  ForeignKey,
  BelongsTo,
} from 'sequelize-typescript';
import { User } from './user.model';

@Table
export class UsersDetails extends Model {
  @Column({ primaryKey: true, autoIncrement: true })
  id: number;

  @Column
  gender: string;

  @Column
  mobileNumber: string;

  @Column
  date_of_birth: string;

  @Column
  city: string;

  @ForeignKey(() => User)
  @Column
  userId: number;

  @BelongsTo(() => User, 'userId')
  user: User;
}
