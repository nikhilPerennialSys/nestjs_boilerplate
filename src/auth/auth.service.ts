import {
  BadRequestException,
  Inject,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import * as bcrypt from 'bcryptjs';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from 'src/core/config/config.service';
import { HTTP_RESPONSE } from 'src/core/constants/messages/response.messages';
import { CustomResponseService } from 'src/core/cusom-response/custom-response.service';
import { AppLogger } from 'src/core/logger/logger.service';
import {
  ErrorMessage,
  SuccessMessage,
} from '../core/constants/messages/auth.messages';
import { LoginDto, UserRegisterDto } from './dto/user.dto';
import { User } from './models/user.model';
import { JwtPayload } from 'src/core/constants/interfaces/other-interfaces';
import { UsersDetails } from './models/userDetails.model';

@Injectable()
export class AuthService {
  constructor(
    private readonly logger: AppLogger,
    private readonly configService: ConfigService,
    private readonly jwtservice: JwtService,
    @Inject('USER_REPOSITORY')
    private readonly userRepository: typeof User,
    @Inject('USER_DETAILS_REPOSITORY')
    private readonly userDetailsRepository: typeof UsersDetails,
    private readonly customResponseService: CustomResponseService,
  ) {}

  async getAllData() {
    return this.userDetailsRepository.findAll({
      include: ['user']
    });
  }

  createJwtPayload(user: JwtPayload) {
    try {
      this.logger.log('START: createJwtPayload: Create new JWT token');
      const data = {
        email: user.email,
        role: user.role,
      };
      this.logger.success('SUCCESS: New token created successfully');
      return this.jwtservice.sign(data);
    } catch (error) {
      this.logger.error(
        `ERROR: createJwtPayload: ${ErrorMessage.SOMETHING_WENT_WRONG} `,
        error,
      );
      throw new UnauthorizedException(ErrorMessage.SOMETHING_WENT_WRONG);
    }
  }

  // create refresh token valid for one day
  createJwtPayloadRefreshToken(user) {
    try {
      this.logger.log(
        'START: createJwtPayloadRefreshToken: Create refresh token valid for one day',
      );
      const data = {
        email: user.email,
        role: user.role,
      };
      this.logger.success('SUCCESS: New refresh token created successfully');
      return this.jwtservice.sign(data, {
        expiresIn: this.configService.get('REFRESH_TOKEN_EXPIRY'),
      });
    } catch (error) {
      this.logger.error(
        `ERROR: createJwtPayloadRefreshToken: ${ErrorMessage.SOMETHING_WENT_WRONG} `,
        error,
      );
      throw new UnauthorizedException(ErrorMessage.SOMETHING_WENT_WRONG);
    }
  }

  // check user exist or not by email
  async findOneByEmail(email: string): Promise<User> {
    try {
      this.logger.log(
        'START: findOneByEmail: Check user exist or not by email',
      );
      return await this.userRepository.findOne({
        where: {
          email: email,
        },
      });
    } catch (error) {
      console.log('ERROR\n', error);
      this.logger.error(
        `ERROR: findOneByEmail: ${ErrorMessage.SOMETHING_WENT_WRONG} `,
        error,
      );
      throw new UnauthorizedException(ErrorMessage.SOMETHING_WENT_WRONG);
    }
  }

  // User Registration
  async register(userRegisterDto: UserRegisterDto) {
    try {
      this.logger.log(
        `START: register: ${userRegisterDto.firstName} Registration.`,
      );
      const { email } = userRegisterDto;
      const userToAttempt = await this.findOneByEmail(
        email.toLocaleLowerCase(),
      );
      if (!userToAttempt) {
        const data = await this.userRepository.create(userRegisterDto);
        await data.save();
        const { city, gender, mobileNumber, date_of_birth } = userRegisterDto;
        const userDetails = await this.userDetailsRepository.create({
          city,
          gender,
          mobileNumber,
          date_of_birth,
          userId: data.id,
        });
        console.log(userDetails);
        await userDetails.save();
        const jwtPayload = {
          email,
          role: data.role,
        };
        const token = this.createJwtPayload(jwtPayload);
        const refreshToken = this.createJwtPayloadRefreshToken(jwtPayload);
        const payload = {
          token,
          refreshToken,
        };
        return payload;
      } else {
        this.logger.error(
          `ERROR: register: ${ErrorMessage.EMAIL_ALREADY_TAKEN}`,
        );
        throw Error(ErrorMessage.EMAIL_ALREADY_TAKEN);
      }
    } catch (error) {
      this.logger.error(`ERROR: register: ${error.message}`, error.message);
      const errRes = await this.customResponseService.buildErrorResponse(
        HTTP_RESPONSE.BAD_REQUEST,
        error.message,
      );
      throw errRes;
    }
  }

  async loginUser(loginDto: LoginDto) {
    try {
      this.logger.log(`START: Login : ${loginDto.email}.`);
      const { email, password } = loginDto;
      const userToAttempt = await this.findOneByEmail(email);
      if (userToAttempt) {
        const isValid = await bcrypt.compare(password, userToAttempt.password);
        if (isValid) {
          const payload: any = {
            token: this.createJwtPayload(userToAttempt),
            refreshToken: this.createJwtPayloadRefreshToken(userToAttempt),
          };
          Promise.all([payload]);
          this.logger.success('SUCCESS: User validated successfully');
          return this.customResponseService.buildSuccessResponse(
            {
              ...payload,
            },
            SuccessMessage.LOGIN_SUCCESSFUL,
            HTTP_RESPONSE.OK,
          );
        } else {
          const errRes = await this.customResponseService.buildErrorResponse(
            HTTP_RESPONSE.NOT_AUTHORIZED,
            ErrorMessage.INVALID_INPUT,
          );
          this.logger.error(
            'ERROR: validateUserByPassword: Incorrect email or password',
          );
          return errRes;
        }
      } else {
        this.logger.error(
          'ERROR: validateUserByPassword: Invalid email or password',
        );
        const errRes = await this.customResponseService.buildErrorResponse(
          HTTP_RESPONSE.NOT_AUTHORIZED,
          ErrorMessage.INVALID_INPUT,
        );
        return errRes;
      }
    } catch (error) {
      this.logger.error(
        'ERROR: validateUserByPassword: User details not found',
        error.message,
      );
      const errRes = await this.customResponseService.buildErrorResponse(
        HTTP_RESPONSE.BAD_REQUEST,
        error.message,
      );
      throw new BadRequestException(errRes);
    }
  }

  // issue new token
  async refreshToken(token: any) {
    try {
      this.logger.log('START: refreshToken: Issue new token');
      const newJWTToken = this.jwtservice.verify(token);
      const data: JwtPayload = {
        email: newJWTToken.email,
        role: newJWTToken.role,
      };
      this.logger.success('SUCCESS: Issued new token successfully');
      const result = {
        token: this.jwtservice.sign(data),
      };
      return this.customResponseService.buildSuccessResponse(
        {
          ...result,
        },
        SuccessMessage.NEW_TOKEN,
        HTTP_RESPONSE.OK,
      );
    } catch (error) {
      const errRes = await this.customResponseService.buildErrorResponse(
        HTTP_RESPONSE.BAD_REQUEST,
        error.message,
      );
      throw new BadRequestException(errRes);
    }
  }
}
