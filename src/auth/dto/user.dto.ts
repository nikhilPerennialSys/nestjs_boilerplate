import { ApiProperty } from '@nestjs/swagger';
import {
  IsEmail,
  IsMobilePhone,
  IsNotEmpty,
  IsNumber,
  IsString,
  Matches,
} from 'class-validator';

export class UserRegisterDto {
  @IsNotEmpty({ message: 'First Name is required.' })
  @ApiProperty()
  firstName: string;

  @IsNotEmpty({ message: 'Last Name is required.' })
  @ApiProperty()
  lastName: string;

  @IsNumber(
    { allowInfinity: false, allowNaN: false },
    { message: 'Age is required.' },
  )
  @ApiProperty()
  age: number;

  @IsNotEmpty({})
  @IsEmail({}, { message: 'You have entered incorrect email address' })
  @ApiProperty()
  email: string;

  @Matches(
    /^(?!.*\s)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[~`!@#$%^&*()--+={}\[\]|\\:;"'<>,.?/_₹]).{8}$/,
    {
      message:
        'Password should be at least 8 characters long, must contain at least 1 number, 1 letter and 1 special character',
    },
  )
  @ApiProperty()
  password: string;

  @ApiProperty()
  gender: string;

  @ApiProperty()
  @IsMobilePhone('en-IN', null, { message: 'Invalid Mobile Number' })
  mobileNumber: string;

  @ApiProperty()
  @IsString({ message: 'Date of Birth is required' })
  date_of_birth: string;

  @ApiProperty()
  city: string;
}

export class LoginDto {
  @IsNotEmpty({})
  @IsEmail({}, { message: 'You have entered incorrect email address' })
  @ApiProperty()
  email: string;

  @Matches(
    /^(?!.*\s)(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[~`!@#$%^&*()--+={}\[\]|\\:;"'<>,.?/_₹]).{8}$/,
    {
      message:
        'Password should be at least 8 characters long, must contain at least 1 number, 1 letter and 1 special character',
    },
  )
  @ApiProperty()
  password: string;
}
