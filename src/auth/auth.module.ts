import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { JwtModule } from '@nestjs/jwt';
import { ConfigModule } from 'src/core/config/config.module';
import { ConfigService } from 'src/core/config/config.service';
import { LoggerModule } from 'src/core/logger/logger.module';
import { AppLogger } from 'src/core/logger/logger.service';
import { AuthController } from './auth.controller';
import { CustomResponseModule } from 'src/core/cusom-response/custom-response.module';
import { CustomResponseService } from 'src/core/cusom-response/custom-response.service';
import { userDetailsProviders, userProviders } from './entity/user.entity';
import { PassportModule } from '@nestjs/passport';
import { JwtStrategy } from './stratergy/jwt.stratergy';

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: 'jwt', session: true }),
    JwtModule.registerAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => ({
        secret: configService.get('JWT_SECRET'),
        signOptions: {
          expiresIn: configService.get('EXPIRES_IN'),
        },
      }),
      inject: [ConfigService],
    }),
    LoggerModule,
    ConfigModule,
    CustomResponseModule,
  ],
  providers: [
    AuthService,
    AppLogger,
    CustomResponseService,
    JwtStrategy,
    ...userProviders,
    ...userDetailsProviders
  ],
  exports: [AuthService],
  controllers: [AuthController],
})
export class AuthModule {}
