import { User } from "../models/user.model";
import { UsersDetails } from "../models/userDetails.model";

export const userProviders = [
  {
    provide: 'USER_REPOSITORY',
    useValue: User,
  },
];

export const userDetailsProviders = [
  {
    provide: 'USER_DETAILS_REPOSITORY',
    useValue: UsersDetails,
  },
];
