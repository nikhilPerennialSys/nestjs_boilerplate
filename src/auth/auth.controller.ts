import {
  BadRequestException,
  Body,
  Controller,
  Get,
  Post,
  Req,
  Res,
  UseGuards,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { Request } from 'express';
import { ROLES } from 'src/core/constants/interfaces/other-interfaces';
import { SuccessMessage } from 'src/core/constants/messages/auth.messages';
import { HTTP_RESPONSE } from 'src/core/constants/messages/response.messages';
import { CustomResponseService } from 'src/core/cusom-response/custom-response.service';
import { Roles } from 'src/core/decorators/role.decorator';
import { JwtAuthGuard } from 'src/core/guards/jwt-auth.guard';
import { RolesGuard } from 'src/core/guards/role.guard';
import { AppLogger } from 'src/core/logger/logger.service';
import { AuthService } from './auth.service';
import { LoginDto, UserRegisterDto } from './dto/user.dto';

@Controller('auth')
@ApiTags('Authentication')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly logger: AppLogger,
    private readonly customResponseService: CustomResponseService,
  ) {}

  @Roles(ROLES.USER, ROLES.ADMIN)
  @UseGuards(RolesGuard)
  @UseGuards(JwtAuthGuard)
  @Get()
  async getAllData() {
    const data = await this.authService.getAllData();
    return this.customResponseService.buildSuccessResponse(
      data,
      'All Data',
      HTTP_RESPONSE.OK,
    );
  }

  @ApiOperation({ summary: 'Register User.' })
  @Post('/signup')
  async userSignUp(@Body() userRegisterDto: UserRegisterDto) {
    try {
      const status = await this.authService.register(userRegisterDto);
      return this.customResponseService.buildSuccessResponse(
        {
          ...status,
        },
        SuccessMessage.REGISTER_SUCCESSFUL,
        HTTP_RESPONSE.CREATED,
      );
    } catch (error) {
      this.logger.error('ERROR: userSignUp : User SignUp', error);
      const err = this.customResponseService.buildErrorResponse(
        { ...HTTP_RESPONSE.BAD_REQUEST, data: error.data },
        error.message,
      );
      throw new BadRequestException(err);
    }
  }

  @ApiOperation({ summary: 'login' })
  @Post('/login')
  async signIn(@Body() loginDto: LoginDto) {
    try {
      const result = await this.authService.loginUser(loginDto);
      return result;
    } catch (error) {
      const errRes = await this.customResponseService.buildErrorResponse(
        HTTP_RESPONSE.BAD_REQUEST,
        error.message,
      );
      throw new BadRequestException(errRes);
    }
  }

  @ApiOperation({ summary: 'refresh' })
  @Get('/refresh')
  async refreshToken(@Req() request: Request) {
    try {
      this.logger.log(`Received refresh token API request`);
      if (!request.headers['authorization']) {
        throw new Error('Unauthorized');
      }
      const result = await this.authService.refreshToken(
        request.headers['authorization'].replace('Bearer ', ''),
      );
      return result;
    } catch (error) {
      const errRes = await this.customResponseService.buildErrorResponse(
        HTTP_RESPONSE.BAD_REQUEST,
        error.message,
      );
      throw new BadRequestException(errRes);
    }
  }
}
