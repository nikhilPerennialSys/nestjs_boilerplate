import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { FallbackExceptionFilter } from './core/filter/fallback.filter';
import { HttpExceptionFilter } from './core/filter/http.filter';
import { AppLogger } from './core/logger/logger.service';
import { createDocument } from './core/swagger/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  app.enableCors();

  app.useGlobalFilters(
    new FallbackExceptionFilter(new AppLogger()),
    new HttpExceptionFilter(new AppLogger()),
  );

  app.useGlobalPipes(new ValidationPipe());
  const port = AppModule.port || 3000;

  SwaggerModule.setup('/api/swagger', app, createDocument(app));

  await app.listen(port, () => {
    console.log(`Server started on http://localhost:${port}`);
  });
}

bootstrap();
