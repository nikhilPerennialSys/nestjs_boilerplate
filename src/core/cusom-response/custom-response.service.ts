/**
 * * Dependencies
 */
import { Injectable } from '@nestjs/common';

/**
 * * Services
 */
import { ConfigService } from '../config/config.service';
import { CustomResponse } from '../constants/interfaces/custom-res.interface';

/**
 * * Types
 */

@Injectable()
export class CustomResponseService {
  constructor(private readonly configService: ConfigService) {
    console.log('Service Init');
  }

  // * build err res
  buildErrorResponse(
    payload: { code: number; statusText: string, data?:string[] | string },
    message?: string | string[],
  ): CustomResponse<any> {
    return {
      status: payload.statusText,
      data: payload.data ? payload.data : null,
      code: payload.code,
      message,
    };
  }

  // * build warnig msg
  buildWarningResponse(
    payload: { code: number; statusText: string },
    message?: string | string[],
  ): CustomResponse<null> {
    return {
      status: payload.statusText,
      data: null,
      code: payload.code,
      message,
    };
  }

  // * build success res
  buildSuccessResponse(
    data: any = '',
    message: string,
    payload: { code: number; statusText: string },
  ): CustomResponse<any> {
    return {
      status: payload.statusText,
      data,
      message,
      code: payload.code,
    };
  }
}
