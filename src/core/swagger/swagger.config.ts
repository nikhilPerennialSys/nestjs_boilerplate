import { SwaggerConfig } from './swagger.interface';

/**
 * Configuration for the swagger UI (found at /api).
 * Change this to suit your app!
 */
export const SWAGGER_CONFIG: SwaggerConfig = {
  title: 'Nest JS Boilerplate',
  description: 'Dummy apis',
  version: '1.0',
  tags:[]
};