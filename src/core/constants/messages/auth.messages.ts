export enum ErrorMessage {
  INVALID_CREDENTIALS = 'Invalid credentials',
  PASSWORD_NOT_MATCH = "Password don't match",
  EMAIL_NOT_FOUND = 'You have entered incorrect email address',
  INVALID_INPUT = 'You have entered incorrect email or password',
  SOMETHING_WENT_WRONG = 'Something went wrong',
  FAIL_TO_SEND_MESSAGE = 'Fail to send Password Reset Link Sent To Your Email:',
  INVALID_TOKEN = 'Invalid token or token expired',
  USER_NOT_FOUND = 'User not found',
  LINK_EXPIRED = 'Link valid for one time use',
  EMAIL_ALREADY_TAKEN = 'This email is already taken.',
  BRAND_NOT_FOUND = 'Brand not Found ',
  REVIEW_NOT_FOUND = 'Reviews not found',
  INCORRECT_TOKEN = 'Token is incorrect',
}

export enum SuccessMessage {
  REGISTER_SUCCESSFUL = 'Account Created Successfully',
  LOGIN_SUCCESSFUL = 'Login successful',
  PASSWORD_RESET_LINK = 'Password Reset Link Sent To Your Email:',
  NEW_TOKEN = 'Issued new token successfully',
  TOKEN_VERIFIED = 'Token verified successfully',
  PASSWORD_UPDATED = 'Password updated successfully',
  REVIEW_ADDED = 'Review added successfully',
  REVIEW_FETCHED = 'Review fetched successfully',
  REVIEW_UPDATED = 'Review updated successfully',
}
