import { ResponseStatus } from '../messages/response.messages';

export interface CustomResponse<T> {
  status: ResponseStatus | string;
  data: T;
  message: string | string[];
  code: number;
}
