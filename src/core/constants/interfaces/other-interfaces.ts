export interface JwtPayload {
  email: string;
  role: string;
}

export enum ROLES {
  ADMIN = 'ADMIN',
  USER = 'USER',
}

export interface Request_USER {
  email: string;
  firstName: string;
  lastName: string;
  role: string;
}
