import { Injectable } from '@nestjs/common';
import { ConfigService } from '../config/config.service';
import { Sequelize } from 'sequelize-typescript';
import { Dialect } from 'sequelize/types';
import { User } from 'src/auth/models/user.model';
import { UsersDetails } from 'src/auth/models/userDetails.model';

@Injectable()
export class DatabaseService {
  constructor(private readonly configService: ConfigService) {}

  async connectDatabase() {
    const SequelizeOptions = {
      dialect: this.configService.get('dialect') as Dialect,
      host: this.configService.get('host'),
      port: parseInt(this.configService.get('port')),
      username: this.configService.get('username'),
      password: this.configService.get('password'),
      database: this.configService.get('database'),
    };
    const sequelize = new Sequelize({
      ...SequelizeOptions,
    });
    sequelize.addModels([UsersDetails, User]);
    console.log(await sequelize.showAllSchemas({ logging: true }));
    await sequelize.sync({
      force: true,
    });
    return sequelize;
  }
}
