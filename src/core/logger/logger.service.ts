import { LoggerService } from '@nestjs/common';
import * as winston from 'winston';

export class AppLogger implements LoggerService {
  private logger: winston.Logger;
  constructor(label?: string) {
    this.initializeLogger(label);
  }
  initializeLogger(label?: string) {
    this.logger = winston.createLogger({
      level: 'info',
      transports: [
        new winston.transports.File({
          filename: './logs/warn.log',
          level: 'warn',
          format: winston.format.combine(winston.format.timestamp()),
        }),
        new winston.transports.Console({
          level: 'warn',
          format: winston.format.combine(winston.format.timestamp()),
        }),
        new winston.transports.File({
          filename: './logs/error.log',
          level: 'error',
          format: winston.format.combine(winston.format.timestamp()),
        }),
        new winston.transports.Console({
          level: 'error',
          format: winston.format.combine(winston.format.timestamp()),
        }),
        new winston.transports.File({
          filename: './logs/info.log',
          level: 'info',
          format: winston.format.combine(winston.format.timestamp()),
        }),
        new winston.transports.Console({
          level: 'info',
          format: winston.format.combine(winston.format.timestamp()),
        }),
        new winston.transports.File({
          filename: './logs/success.log',
          level: 'success',
          format: winston.format.combine(winston.format.timestamp()),
        }),
        new winston.transports.Console({
          level: 'success',
          format: winston.format.combine(winston.format.timestamp()),
        }),
        new winston.transports.File({
          filename: './logs/failure.log',
          level: 'failure',
          format: winston.format.combine(winston.format.timestamp()),
        }),
        new winston.transports.Console({
          level: 'failure',
          format: winston.format.combine(winston.format.timestamp()),
        }),
      ],
    });
  }
  error(message: string, trace: any = {}) {
    this.logger.error(
      `${new Date().toLocaleString()} ${message} ,
      ${JSON.stringify(trace)}`,
    );
  }

  warn(message: string) {
    this.logger.warn('warn', `${new Date().toLocaleString()} ${message}`);
  }

  log(message: string, data: any = {}) {
    this.logger.log(
      'info',
      `${new Date().toLocaleString()}  ${message}, data : ${JSON.stringify(
        data,
      )}`,
    );
  }

  success(message: string) {
    this.logger.log('info', `${new Date().toLocaleString()}  ${message}`);
  }

  failure(message: string) {
    this.logger.error('error', `${new Date().toLocaleString()}  ${message}`);
  }
}
