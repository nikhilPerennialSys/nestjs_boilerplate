import { ArgumentsHost, Catch, ExceptionFilter } from '@nestjs/common';
import { Request, Response } from 'express';
import { AppLogger } from '../logger/logger.service';

@Catch()
export class FallbackExceptionFilter implements ExceptionFilter {
  constructor(private logger: AppLogger) {}
  catch(exception: any, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    this.logger.error(
      `FALLBACK_EXCEPTION : ${exception.message}`,
      'exception',
    );

    return response.status(500).json({
      statusCode: 500,
      createdBy: 'FallbackExceptionFilter',
      errorMessage: exception.message
        ? exception.message
        : 'Unexpected error ocurred',
      timestamp: new Date().toISOString(),
      path: request.url,
    });
  }
}
