import {
  ArgumentsHost,
  Catch,
  ExceptionFilter,
  HttpException,
} from '@nestjs/common';
import { Request, Response } from 'express';
import { AppLogger } from '../logger/logger.service';

@Catch(HttpException)
export class HttpExceptionFilter implements ExceptionFilter {
  constructor(private appLogger: AppLogger) {}
  catch(exception: HttpException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const valError: any = exception.getResponse();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest<Request>();
    const status = exception.getStatus();
    this.appLogger.error(
      `HTTP_EXCEPTION : ${exception.message}`,
      exception.message,
    );
    return response.status(status).json({
      timestamp: new Date().toISOString(),
      createdBy: 'HttpExceptionFilter',
      path: request.url,
      code: valError.statusCode || status,
      status: valError?.error || 'Bad Request',
      message: valError.data
        ? valError.data
        : valError?.message
        ? valError?.message
        : null,
      data: null,
    });
  }
}
