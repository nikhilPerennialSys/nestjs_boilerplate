import { Module } from '@nestjs/common';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './core/config/config.module';
import { ConfigService } from './core/config/config.service';
import { LoggerModule } from './core/logger/logger.module';
import { AppLogger } from './core/logger/logger.service';
import { DatabaseModule } from './core/database/database.module';
import { DatabaseService } from './core/database/database.service';

@Module({
  imports: [AuthModule, ConfigModule, LoggerModule, DatabaseModule],
  providers: [AppLogger],
})
export class AppModule {
  static port: number | string;
  constructor(
    private _configService: ConfigService,
    private _databaseService: DatabaseService,
    private appLogger: AppLogger,
  ) {
    AppModule.port = this._configService.get('PORT');
    this._databaseService.connectDatabase();
    this.appLogger.log(`AppModule.port, ${AppModule.port}`);
  }
}
